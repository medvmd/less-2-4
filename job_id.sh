#!/bin/bash
for pipeline in $(curl -k --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/46399960/pipelines" | jq '.[] | select(.status=="success") | .id'); do curl -k --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/46399960/pipelines/$pipeline/jobs" | jq '.[] | select (.status =="success") | select (.name=="build_prod")'
done | grep -B3 'build_prod' | grep id | grep -oP '(?<=id": )\d+' | sort -r | head -n 1
